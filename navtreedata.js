/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ghosh_Project", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Lab 0x01", "index.html#sec_lab1", null ],
    [ "Lab 0x02", "index.html#sec_lab2", null ],
    [ "Lab 0x03", "index.html#sec_lab3", null ],
    [ "Lab 0x04", "index.html#sec_lab4", null ],
    [ "Lab 0x05", "index.html#sec_lab5", null ],
    [ "Lab 0xFF", "index.html#sec_lab_ff", null ],
    [ "HW 0x02", "index.html#sec_hw2", null ],
    [ "HW 0x03", "index.html#sec_hw3", null ],
    [ "Lab 0x04 Testing Results", "lab_4_testing.html", [
      [ "Plots", "lab_4_testing.html#sec_plots", null ],
      [ "Block Diagram", "lab_4_testing.html#sec_block", null ],
      [ "Tasks", "lab_4_testing.html#sec_tasks", null ]
    ] ],
    [ "HW 0xFF Report", "hw_0xff.html", [
      [ "Torque to PWM Conversion", "hw_0xff.html#sec_pwm", null ],
      [ "Gain Calculations", "hw_0xff.html#gain_calc", null ],
      [ "Task Diagram", "hw_0xff.html#sec_tasksf", null ]
    ] ],
    [ "HW 0x02 Calculations", "hw_0x02.html", null ],
    [ "HW 0x03 Plots", "hw_0x03.html", [
      [ "Source Code", "hw_0x03.html#sec_source", null ],
      [ "Simulation 1 - Open-Loop Part a", "hw_0x03.html#sec_sim1", null ],
      [ "Simulation 2 - Open-Loop Part b", "hw_0x03.html#sec_sim2", null ],
      [ "Simulation 3 - Closed-Loop", "hw_0x03.html#sec_sim3", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ],
      [ "Package Functions", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ]
      ] ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"HW__0x03__sim_8py.html",
"Lab__0xFF__closedloop_8py.html",
"classLab__0x03__task__motor_1_1Task__Motor.html#ae8f00b1804e9245aa9ce3a04f1993e8b",
"classLab__0xFF__imu__driver_1_1IMU__Driver.html",
""
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';