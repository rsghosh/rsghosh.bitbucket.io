var files_dup =
[
    [ "HW_0x03_sim.py", "HW__0x03__sim_8py.html", "HW__0x03__sim_8py" ],
    [ "Lab_0x01.py", "Lab__0x01_8py.html", "Lab__0x01_8py" ],
    [ "Lab_0x02_encoder.py", "Lab__0x02__encoder_8py.html", [
      [ "Lab_0x02_encoder.Encoder", "classLab__0x02__encoder_1_1Encoder.html", "classLab__0x02__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x02_main.py", "Lab__0x02__main_8py.html", "Lab__0x02__main_8py" ],
    [ "Lab_0x02_task_encoder.py", "Lab__0x02__task__encoder_8py.html", "Lab__0x02__task__encoder_8py" ],
    [ "Lab_0x02_task_user.py", "Lab__0x02__task__user_8py.html", "Lab__0x02__task__user_8py" ],
    [ "Lab_0x03_drv8847.py", "Lab__0x03__drv8847_8py.html", "Lab__0x03__drv8847_8py" ],
    [ "Lab_0x03_encoder.py", "Lab__0x03__encoder_8py.html", [
      [ "Lab_0x03_encoder.Encoder", "classLab__0x03__encoder_1_1Encoder.html", "classLab__0x03__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x03_main.py", "Lab__0x03__main_8py.html", "Lab__0x03__main_8py" ],
    [ "Lab_0x03_task_encoder.py", "Lab__0x03__task__encoder_8py.html", "Lab__0x03__task__encoder_8py" ],
    [ "Lab_0x03_task_motor.py", "Lab__0x03__task__motor_8py.html", "Lab__0x03__task__motor_8py" ],
    [ "Lab_0x03_task_user.py", "Lab__0x03__task__user_8py.html", "Lab__0x03__task__user_8py" ],
    [ "Lab_0x04_closedloop.py", "Lab__0x04__closedloop_8py.html", [
      [ "Lab_0x04_closedloop.ClosedLoop", "classLab__0x04__closedloop_1_1ClosedLoop.html", "classLab__0x04__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab_0x04_drv8847.py", "Lab__0x04__drv8847_8py.html", "Lab__0x04__drv8847_8py" ],
    [ "Lab_0x04_encoder.py", "Lab__0x04__encoder_8py.html", [
      [ "Lab_0x04_encoder.Encoder", "classLab__0x04__encoder_1_1Encoder.html", "classLab__0x04__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x04_main.py", "Lab__0x04__main_8py.html", "Lab__0x04__main_8py" ],
    [ "Lab_0x04_task_encoder.py", "Lab__0x04__task__encoder_8py.html", "Lab__0x04__task__encoder_8py" ],
    [ "Lab_0x04_task_motor.py", "Lab__0x04__task__motor_8py.html", "Lab__0x04__task__motor_8py" ],
    [ "Lab_0x04_task_user.py", "Lab__0x04__task__user_8py.html", "Lab__0x04__task__user_8py" ],
    [ "Lab_0x05.py", "Lab__0x05_8py.html", "Lab__0x05_8py" ],
    [ "Lab_0xFF_closedloop.py", "Lab__0xFF__closedloop_8py.html", [
      [ "Lab_0xFF_closedloop.ClosedLoop", "classLab__0xFF__closedloop_1_1ClosedLoop.html", "classLab__0xFF__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab_0xFF_drv8847.py", "Lab__0xFF__drv8847_8py.html", "Lab__0xFF__drv8847_8py" ],
    [ "Lab_0xFF_imu_driver.py", "Lab__0xFF__imu__driver_8py.html", "Lab__0xFF__imu__driver_8py" ],
    [ "Lab_0xFF_main.py", "Lab__0xFF__main_8py.html", "Lab__0xFF__main_8py" ],
    [ "Lab_0xFF_task_collection.py", "Lab__0xFF__task__collection_8py.html", "Lab__0xFF__task__collection_8py" ],
    [ "Lab_0xFF_task_imu.py", "Lab__0xFF__task__imu_8py.html", "Lab__0xFF__task__imu_8py" ],
    [ "Lab_0xFF_task_motor.py", "Lab__0xFF__task__motor_8py.html", "Lab__0xFF__task__motor_8py" ],
    [ "Lab_0xFF_task_touch_panel.py", "Lab__0xFF__task__touch__panel_8py.html", "Lab__0xFF__task__touch__panel_8py" ],
    [ "Lab_0xFF_task_user.py", "Lab__0xFF__task__user_8py.html", "Lab__0xFF__task__user_8py" ],
    [ "Lab_0xFF_touch_panel.py", "Lab__0xFF__touch__panel_8py.html", [
      [ "Lab_0xFF_touch_panel.Touch_Panel", "classLab__0xFF__touch__panel_1_1Touch__Panel.html", "classLab__0xFF__touch__panel_1_1Touch__Panel" ]
    ] ],
    [ "mainpage.py", "mainpage_8py.html", null ]
];