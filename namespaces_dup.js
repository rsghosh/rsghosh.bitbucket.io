var namespaces_dup =
[
    [ "HW_0x03_sim", null, [
      [ "get_input", "HW__0x03__sim_8py.html#af29ca0e3937b8dbe1139a23ce1e82679", null ],
      [ "make_plots", "HW__0x03__sim_8py.html#af11e4ee1a6cb0ffb1e3ca5c08629209e", null ],
      [ "sim", "HW__0x03__sim_8py.html#ae6f2d06a0890cce091e6f9d18eb7158d", null ],
      [ "A", "HW__0x03__sim_8py.html#a59dd1e40dbb48f3292aed5adea0bd87b", null ],
      [ "B", "HW__0x03__sim_8py.html#ad5d0e0e2107a0121c24db5c944809ede", null ],
      [ "duration", "HW__0x03__sim_8py.html#a191e2376611d15fb3e86bfc7599f4a99", null ],
      [ "K", "HW__0x03__sim_8py.html#ad34880f80ebd49eb07a4a55d2c1c521b", null ],
      [ "t_inc", "HW__0x03__sim_8py.html#af9ebc07ef339dc019ede0a7d9dddd670", null ],
      [ "u0", "HW__0x03__sim_8py.html#a277d24551b18be424f2cec1a709e5543", null ],
      [ "x0", "HW__0x03__sim_8py.html#a0cc43a766267c122710296bf0e96d92f", null ]
    ] ],
    [ "Lab_0x01", "namespaceLab__0x01.html", [
      [ "onButtonPressFCN", "namespaceLab__0x01.html#af3eb145e62c28b9e105562736c2e1a10", null ],
      [ "update_sqw", "namespaceLab__0x01.html#a8e40cba9481d750ddb6f184aa3f4a090", null ],
      [ "update_stw", "namespaceLab__0x01.html#a38a2edcac03940dce2585957bc593842", null ],
      [ "update_sw", "namespaceLab__0x01.html#a3ffad3b8c0126e18d2d8f26d809b4cdd", null ],
      [ "update_wave", "namespaceLab__0x01.html#a7c544c46b0ec0f69613874707c34555d", null ],
      [ "button_pressed", "namespaceLab__0x01.html#afd5d1ec4fb001d7c35bb739edb25aa86", null ],
      [ "ButtonInt", "namespaceLab__0x01.html#a87006ae4b27231f7f74a3813aed69285", null ],
      [ "pinA5", "namespaceLab__0x01.html#ad3e0897a46d11dcb8e7c80cfb890e94e", null ],
      [ "pinC13", "namespaceLab__0x01.html#ab571df62ccbdaef2192191c5708d34cc", null ],
      [ "ref", "namespaceLab__0x01.html#a59b51d1c0a6cd0e5b53c9376797415b1", null ],
      [ "state", "namespaceLab__0x01.html#aff4fbbfc19bd0ef7e98b8d1d2ca9c486", null ],
      [ "t2ch1", "namespaceLab__0x01.html#a0ccd673fe0925ff9f21badc2a9818425", null ],
      [ "tim2", "namespaceLab__0x01.html#adc11003cdc2b39c8f89cb28f85dafc67", null ]
    ] ],
    [ "Lab_0x02_encoder", null, [
      [ "Encoder", "classLab__0x02__encoder_1_1Encoder.html", "classLab__0x02__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x02_main", null, [
      [ "enc1", "Lab__0x02__main_8py.html#a7ff260270ba89cfd6cb4b02ff51171b9", null ],
      [ "pinA", "Lab__0x02__main_8py.html#a1e38001fb4fee716147dd0a46dd07c9a", null ],
      [ "pinB", "Lab__0x02__main_8py.html#a5e10ccce0b8c062281323dd03dec82a2", null ],
      [ "shared_vars", "Lab__0x02__main_8py.html#ab0115c1cd73aa07817dad053e41fe960", null ],
      [ "t4ch1", "Lab__0x02__main_8py.html#acf743722c590371e23945393dbbaee38", null ],
      [ "t4ch2", "Lab__0x02__main_8py.html#a347bbcc991a0816ac8d32449a7c7fb96", null ],
      [ "t_enc", "Lab__0x02__main_8py.html#a932cd55fb3bfbc1467c0831ff2c90baf", null ],
      [ "t_user", "Lab__0x02__main_8py.html#a2fc8af33031a0aa75cc8ac664ac6c611", null ],
      [ "tim4", "Lab__0x02__main_8py.html#acf35997a896cc1b05f386bf5e9a26cfd", null ]
    ] ],
    [ "Lab_0x02_task_encoder", null, [
      [ "Task_Encoder", "classLab__0x02__task__encoder_1_1Task__Encoder.html", "classLab__0x02__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab_0x02_task_user", null, [
      [ "Task_User", "classLab__0x02__task__user_1_1Task__User.html", "classLab__0x02__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0x03_drv8847", null, [
      [ "DRV8847", "classLab__0x03__drv8847_1_1DRV8847.html", "classLab__0x03__drv8847_1_1DRV8847" ],
      [ "Motor", "classLab__0x03__drv8847_1_1Motor.html", "classLab__0x03__drv8847_1_1Motor" ],
      [ "in1", "Lab__0x03__drv8847_8py.html#af48a06013b37e452ad77196e92da8940", null ],
      [ "in2", "Lab__0x03__drv8847_8py.html#a335940f4597a8339c29a8cc590aeed1c", null ],
      [ "in3", "Lab__0x03__drv8847_8py.html#a7a4190020b89aa99f422489556be1a84", null ],
      [ "in4", "Lab__0x03__drv8847_8py.html#a8a4d19e48a69f9d2995d7d458a7db516", null ],
      [ "motor_1", "Lab__0x03__drv8847_8py.html#a4b54418f39e4f53c785b1ecc210cfccd", null ],
      [ "motor_2", "Lab__0x03__drv8847_8py.html#a63e11136356c665da121a7f593e6a6a3", null ],
      [ "motor_drv", "Lab__0x03__drv8847_8py.html#a2a518f65846e4afb10d351ebdb8c587d", null ],
      [ "nFault", "Lab__0x03__drv8847_8py.html#a2a60fa0c4930667ef679c1084f75369d", null ],
      [ "nSleep", "Lab__0x03__drv8847_8py.html#a4380e7a242d08bc93b07dc2d42d53d5c", null ],
      [ "t3ch1", "Lab__0x03__drv8847_8py.html#a0624f88ebb6b3c33a785df628d860958", null ],
      [ "t3ch2", "Lab__0x03__drv8847_8py.html#ab4099f24becc1d719e947754c7f2a535", null ],
      [ "t3ch3", "Lab__0x03__drv8847_8py.html#a3d8e4a4ffc5dec23631bd12579e8d1cb", null ],
      [ "t3ch4", "Lab__0x03__drv8847_8py.html#abcf92cb760d6efe7060c4d0bff29923f", null ],
      [ "tim3", "Lab__0x03__drv8847_8py.html#a70dca3a04d6ab8b24816ce893c47ebd9", null ]
    ] ],
    [ "Lab_0x03_encoder", null, [
      [ "Encoder", "classLab__0x03__encoder_1_1Encoder.html", "classLab__0x03__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x03_main", null, [
      [ "enc1", "Lab__0x03__main_8py.html#a65783a4bddc15ba6284d644c008c8bd8", null ],
      [ "enc2", "Lab__0x03__main_8py.html#addb5a888d63894a7497666eb03abeb08", null ],
      [ "in1", "Lab__0x03__main_8py.html#a8f3aef7d48218f57053fd3a8d8597cc5", null ],
      [ "in2", "Lab__0x03__main_8py.html#a586a3fc602b7150d7a58b390a82d9976", null ],
      [ "in3", "Lab__0x03__main_8py.html#ad94bf1776452981a910c0ee4156535ac", null ],
      [ "in4", "Lab__0x03__main_8py.html#aa2ea5bbf2851dab0449024f864c31eb2", null ],
      [ "motor_1", "Lab__0x03__main_8py.html#a64c2da1acfae9563011c1e2796fe72ad", null ],
      [ "motor_2", "Lab__0x03__main_8py.html#a696102610a7acb187164249219b95d21", null ],
      [ "motor_drv", "Lab__0x03__main_8py.html#afafec44f274a48c9006591de45314319", null ],
      [ "nFault", "Lab__0x03__main_8py.html#a06086b2f7b46e134861b7826f7fb634a", null ],
      [ "nSleep", "Lab__0x03__main_8py.html#a96d5f778677843b0e140dcaa124826e5", null ],
      [ "pinA1", "Lab__0x03__main_8py.html#a3c37ea82f7d0253755f792502a0a975e", null ],
      [ "pinA2", "Lab__0x03__main_8py.html#ae0a22769f3aaee1dc9e7f600430243b5", null ],
      [ "pinB1", "Lab__0x03__main_8py.html#a88b4bae1cacfe4f6335d4485e3286e31", null ],
      [ "pinB2", "Lab__0x03__main_8py.html#a4571acd32de6e0dd2c02412b8e77b72a", null ],
      [ "shared_vars", "Lab__0x03__main_8py.html#aa2ccab2cfea8e8c9717a44021138eaf4", null ],
      [ "t3ch1", "Lab__0x03__main_8py.html#a6cfec660479900808903f0849c9eda67", null ],
      [ "t3ch2", "Lab__0x03__main_8py.html#a7a390d4df2e3210f6ae35038768e480c", null ],
      [ "t3ch3", "Lab__0x03__main_8py.html#a26ced363ded0877b3bf9eeea8aceaa2f", null ],
      [ "t3ch4", "Lab__0x03__main_8py.html#ad691b7b4387771514aec106255e6e7c1", null ],
      [ "t48h1", "Lab__0x03__main_8py.html#a25d4cc8473473ee812c2c6550a81f346", null ],
      [ "t4ch1", "Lab__0x03__main_8py.html#ab0ea53edc39ea48eab512809f3395265", null ],
      [ "t4ch2", "Lab__0x03__main_8py.html#aaffc50dd1328cae85b50a612f69b1d31", null ],
      [ "t8ch2", "Lab__0x03__main_8py.html#a47774c94a393778928eb15f779bab2d6", null ],
      [ "t_enc", "Lab__0x03__main_8py.html#adcc0ee20a6bef276c915213abf1530ba", null ],
      [ "t_motor", "Lab__0x03__main_8py.html#a969937423609008c729c76f8a3a9b9bd", null ],
      [ "t_user", "Lab__0x03__main_8py.html#a2261bbbd1f1d0ab95b47574b8ae72901", null ],
      [ "tim3", "Lab__0x03__main_8py.html#ab189262a3e93787c985e867aebb4c2e5", null ],
      [ "tim4", "Lab__0x03__main_8py.html#a1d8cb52f7d23f83c17ee35b897d6af7a", null ],
      [ "tim8", "Lab__0x03__main_8py.html#ae43c7e6303ea84dc8f1f1ff229395386", null ]
    ] ],
    [ "Lab_0x03_task_encoder", null, [
      [ "Task_Encoder", "classLab__0x03__task__encoder_1_1Task__Encoder.html", "classLab__0x03__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab_0x03_task_motor", null, [
      [ "Task_Motor", "classLab__0x03__task__motor_1_1Task__Motor.html", "classLab__0x03__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab_0x03_task_user", null, [
      [ "Task_User", "classLab__0x03__task__user_1_1Task__User.html", "classLab__0x03__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0x04_closedloop", null, [
      [ "ClosedLoop", "classLab__0x04__closedloop_1_1ClosedLoop.html", "classLab__0x04__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab_0x04_drv8847", null, [
      [ "DRV8847", "classLab__0x04__drv8847_1_1DRV8847.html", "classLab__0x04__drv8847_1_1DRV8847" ],
      [ "Motor", "classLab__0x04__drv8847_1_1Motor.html", "classLab__0x04__drv8847_1_1Motor" ],
      [ "in1", "Lab__0x04__drv8847_8py.html#acbeec04696c297e305c3179952c2848a", null ],
      [ "in2", "Lab__0x04__drv8847_8py.html#ae1172dcc2b509267aebde33e1d80b818", null ],
      [ "in3", "Lab__0x04__drv8847_8py.html#a7473cd75f901f19b5f38e240da5055fd", null ],
      [ "in4", "Lab__0x04__drv8847_8py.html#a188bb4f6c1893777c317d20bfccc7dc3", null ],
      [ "motor_1", "Lab__0x04__drv8847_8py.html#ab650d0258a54ef590c8beaeb65f772ab", null ],
      [ "motor_2", "Lab__0x04__drv8847_8py.html#a1601d21feba546ba835e3c95c5a349ca", null ],
      [ "motor_drv", "Lab__0x04__drv8847_8py.html#ab0e3564816bb7d50da2dc9c70e238795", null ],
      [ "nFault", "Lab__0x04__drv8847_8py.html#a73f085c1d1c914a0acc105531b6332a2", null ],
      [ "nSleep", "Lab__0x04__drv8847_8py.html#a18f7d54743f9916893e5fa33b8eb3eef", null ],
      [ "t3ch1", "Lab__0x04__drv8847_8py.html#a8d5390af465e10e5f5ae0cdff80c4a80", null ],
      [ "t3ch2", "Lab__0x04__drv8847_8py.html#ae00b8243f7a4298adbffed5c6cf0d1a3", null ],
      [ "t3ch3", "Lab__0x04__drv8847_8py.html#a4d501548b883818cc480a623e8e9036d", null ],
      [ "t3ch4", "Lab__0x04__drv8847_8py.html#a3e8dddcd377623388da546d8e156cb3d", null ],
      [ "tim3", "Lab__0x04__drv8847_8py.html#a54051e5864825727991b66f6e0d223c9", null ]
    ] ],
    [ "Lab_0x04_encoder", null, [
      [ "Encoder", "classLab__0x04__encoder_1_1Encoder.html", "classLab__0x04__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x04_main", null, [
      [ "enc1", "Lab__0x04__main_8py.html#a19af4ac94507ee1ddc662469f0c3c057", null ],
      [ "enc2", "Lab__0x04__main_8py.html#a1c43848ca7919b052c64bc062f356e35", null ],
      [ "in1", "Lab__0x04__main_8py.html#a1e67689d52ff3fbc279911d5f73e7668", null ],
      [ "in2", "Lab__0x04__main_8py.html#a50a831ba1cb84f35cd7cf0bb7e882905", null ],
      [ "in3", "Lab__0x04__main_8py.html#adc00996c108806fd2a2ffea054acc43a", null ],
      [ "in4", "Lab__0x04__main_8py.html#a33eb2673ebb5f71676dec8c2dfd9779a", null ],
      [ "motor_1", "Lab__0x04__main_8py.html#a0c2989c68475afb5ff25e2d8ef0efc54", null ],
      [ "motor_2", "Lab__0x04__main_8py.html#a520d78b382fd3be183313823c374e90f", null ],
      [ "motor_drv", "Lab__0x04__main_8py.html#ae8298b73de2fe8bf3c97ea4c139891c5", null ],
      [ "nFault", "Lab__0x04__main_8py.html#a48c703b2b0010e95182b1eb389f4565c", null ],
      [ "nSleep", "Lab__0x04__main_8py.html#abf540f5c81830faf2846516e7839f98a", null ],
      [ "pinA1", "Lab__0x04__main_8py.html#ac7966b5360bff790630a907723f80f90", null ],
      [ "pinA2", "Lab__0x04__main_8py.html#addf5b205677564baa7bf30155faab28a", null ],
      [ "pinB1", "Lab__0x04__main_8py.html#a4e5806ab6b6a5deadac727f10abc499e", null ],
      [ "pinB2", "Lab__0x04__main_8py.html#a77cd73679a529662a506e78ab4b7f24e", null ],
      [ "shared_vars", "Lab__0x04__main_8py.html#a7b5eb18e2c5fd1b5595d075274918587", null ],
      [ "t3ch1", "Lab__0x04__main_8py.html#a02bf031722eec4f0b9a8a4f8f93f827d", null ],
      [ "t3ch2", "Lab__0x04__main_8py.html#adabdfc1f16911a6ab13cba36b0e3c660", null ],
      [ "t3ch3", "Lab__0x04__main_8py.html#abea5d2c7e6331e8625a3f3c81617cef9", null ],
      [ "t3ch4", "Lab__0x04__main_8py.html#a5bd56025d6aefc169e17db62793b104e", null ],
      [ "t48h1", "Lab__0x04__main_8py.html#af03a93a645fc5e04b78c93095d59b891", null ],
      [ "t4ch1", "Lab__0x04__main_8py.html#a9272b69339f6509e0ffe0d699433027f", null ],
      [ "t4ch2", "Lab__0x04__main_8py.html#a92d0ed3152d8a505ce170b9fb3221124", null ],
      [ "t8ch2", "Lab__0x04__main_8py.html#a62683c6dede0b020be2edc6c06713500", null ],
      [ "t_enc", "Lab__0x04__main_8py.html#aec0c0a89f5b64d561c411cc05190a62c", null ],
      [ "t_motor", "Lab__0x04__main_8py.html#a21143b9c92d2150393fa2ea36108f439", null ],
      [ "t_user", "Lab__0x04__main_8py.html#ab02f90242478f6a7027cf71894e38b02", null ],
      [ "tim3", "Lab__0x04__main_8py.html#ad3286471265341968de9b5578751e3bf", null ],
      [ "tim4", "Lab__0x04__main_8py.html#a951e9c9afafc0156f76b081dcd17a119", null ],
      [ "tim8", "Lab__0x04__main_8py.html#a253292412bb3dea00ab35bcc846142fc", null ]
    ] ],
    [ "Lab_0x04_task_encoder", null, [
      [ "Task_Encoder", "classLab__0x04__task__encoder_1_1Task__Encoder.html", "classLab__0x04__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab_0x04_task_motor", null, [
      [ "Task_Motor", "classLab__0x04__task__motor_1_1Task__Motor.html", "classLab__0x04__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab_0x04_task_user", null, [
      [ "Task_User", "classLab__0x04__task__user_1_1Task__User.html", "classLab__0x04__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0x05", "namespaceLab__0x05.html", "namespaceLab__0x05" ],
    [ "Lab_0xFF_closedloop", null, [
      [ "ClosedLoop", "classLab__0xFF__closedloop_1_1ClosedLoop.html", "classLab__0xFF__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab_0xFF_drv8847", null, [
      [ "DRV8847", "classLab__0xFF__drv8847_1_1DRV8847.html", "classLab__0xFF__drv8847_1_1DRV8847" ],
      [ "Motor", "classLab__0xFF__drv8847_1_1Motor.html", "classLab__0xFF__drv8847_1_1Motor" ],
      [ "in1", "Lab__0xFF__drv8847_8py.html#a1a42f91acd32f9c457d87dd8940d65d7", null ],
      [ "in2", "Lab__0xFF__drv8847_8py.html#a7867b730fd655e87c8a20cc19171b14a", null ],
      [ "in3", "Lab__0xFF__drv8847_8py.html#a04c549790cb86141ff2ca96ded4090df", null ],
      [ "in4", "Lab__0xFF__drv8847_8py.html#a6525721fa4a414186b9b3cb1fe0bd0bb", null ],
      [ "motor_1", "Lab__0xFF__drv8847_8py.html#addebb8bb6dfb94ed95b10b10ad3084f1", null ],
      [ "motor_2", "Lab__0xFF__drv8847_8py.html#a0ed2819e5541c1f4fcf957fa5605902f", null ],
      [ "motor_drv", "Lab__0xFF__drv8847_8py.html#a7f5efe4b6fbc8da82c9cd5f7a79fa99f", null ],
      [ "nFault", "Lab__0xFF__drv8847_8py.html#ab09d3840a09515f3dcac184a67ad9000", null ],
      [ "nSleep", "Lab__0xFF__drv8847_8py.html#a47dc7688713d981807596c141480ca4b", null ],
      [ "t3ch1", "Lab__0xFF__drv8847_8py.html#ac3edf93255b72c8a2cb8bc5d52ca26a6", null ],
      [ "t3ch2", "Lab__0xFF__drv8847_8py.html#a45285c83dcd0116682f83bf47a12fba6", null ],
      [ "t3ch3", "Lab__0xFF__drv8847_8py.html#a3b3ab0e444252ba53535a895564f947f", null ],
      [ "t3ch4", "Lab__0xFF__drv8847_8py.html#a301d6875ac2cb132b30176dc834d4fa5", null ],
      [ "tim3", "Lab__0xFF__drv8847_8py.html#aeaed8235dd2f128aa90405ac8c582d22", null ]
    ] ],
    [ "Lab_0xFF_imu_driver", null, [
      [ "IMU_Driver", "classLab__0xFF__imu__driver_1_1IMU__Driver.html", "classLab__0xFF__imu__driver_1_1IMU__Driver" ]
    ] ],
    [ "Lab_0xFF_main", null, [
      [ "imu", "Lab__0xFF__main_8py.html#a7ae63e0bc62c6d88f117c4ff93168f2c", null ],
      [ "in1", "Lab__0xFF__main_8py.html#ac1996981a0b0603579aa77abcd0f1fd8", null ],
      [ "in2", "Lab__0xFF__main_8py.html#a82f9cdcccda3447e6dd460b4b58da156", null ],
      [ "in3", "Lab__0xFF__main_8py.html#a8ef0a7c65eff17f60c9a8a909a2ff358", null ],
      [ "in4", "Lab__0xFF__main_8py.html#add8644e8434fb6c0770f8055017c000d", null ],
      [ "K1", "Lab__0xFF__main_8py.html#a2314b9e5a5a3953eba5dfb64df174fc5", null ],
      [ "K2", "Lab__0xFF__main_8py.html#a4095797cdbcc57f7a4883a4fbea0398c", null ],
      [ "motor_1", "Lab__0xFF__main_8py.html#a92debfc9d4a763f4b86b87fe9b144343", null ],
      [ "motor_2", "Lab__0xFF__main_8py.html#ab116af079299fe989352507d0b367bfc", null ],
      [ "motor_drv", "Lab__0xFF__main_8py.html#a4638bd579fd569e6f901f57576d26131", null ],
      [ "nFault", "Lab__0xFF__main_8py.html#a0ee1e8e77981c3a695770bc38d1df91e", null ],
      [ "nSleep", "Lab__0xFF__main_8py.html#a3f664ebc8a872831aeec02203465ce46", null ],
      [ "shared_vars", "Lab__0xFF__main_8py.html#a56f73bd071f43f8cf21c815d743838ba", null ],
      [ "t3ch1", "Lab__0xFF__main_8py.html#ab9d42eed89d975f53e01d10cb147dc01", null ],
      [ "t3ch2", "Lab__0xFF__main_8py.html#a96e9252a655a74990d2bc7d84793fa88", null ],
      [ "t3ch3", "Lab__0xFF__main_8py.html#a89e262c91879b1af81973e56fb83c477", null ],
      [ "t3ch4", "Lab__0xFF__main_8py.html#a76f35313d31878d18ab1b4e56a2db623", null ],
      [ "t_collection", "Lab__0xFF__main_8py.html#aa491d14b46f39e93bf099b3e0e1a3012", null ],
      [ "t_imu", "Lab__0xFF__main_8py.html#acfbea04c57006950c1f4db48b7ca0197", null ],
      [ "t_motor", "Lab__0xFF__main_8py.html#a7b766c03d0b2209545fb07ca0da39e90", null ],
      [ "t_touch_panel", "Lab__0xFF__main_8py.html#a5848619e8f529973e6ed7649cb6186b5", null ],
      [ "t_user", "Lab__0xFF__main_8py.html#a54dfefa52f342fe11c00909fc39fbe4d", null ],
      [ "tim3", "Lab__0xFF__main_8py.html#a4cf9a696745c54213cf0b058043c4533", null ],
      [ "tp", "Lab__0xFF__main_8py.html#a2ded5995585a9b7819576f82cf487da6", null ],
      [ "xm", "Lab__0xFF__main_8py.html#a029735be80bb3ee4d9c1ee40c4c943aa", null ],
      [ "xp", "Lab__0xFF__main_8py.html#a079c9f7e1aad72dda876b39489db54de", null ],
      [ "ym", "Lab__0xFF__main_8py.html#a2b69ba2fac8ebed592e09476547211aa", null ],
      [ "yp", "Lab__0xFF__main_8py.html#afdbbc8e32d9100f679a646b1933a1de3", null ]
    ] ],
    [ "Lab_0xFF_task_collection", null, [
      [ "Task_Collection", "classLab__0xFF__task__collection_1_1Task__Collection.html", "classLab__0xFF__task__collection_1_1Task__Collection" ]
    ] ],
    [ "Lab_0xFF_task_imu", null, [
      [ "Task_IMU", "classLab__0xFF__task__imu_1_1Task__IMU.html", "classLab__0xFF__task__imu_1_1Task__IMU" ]
    ] ],
    [ "Lab_0xFF_task_motor", null, [
      [ "Task_Motor", "classLab__0xFF__task__motor_1_1Task__Motor.html", "classLab__0xFF__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab_0xFF_task_touch_panel", null, [
      [ "Task_Touch_Panel", "classLab__0xFF__task__touch__panel_1_1Task__Touch__Panel.html", "classLab__0xFF__task__touch__panel_1_1Task__Touch__Panel" ]
    ] ],
    [ "Lab_0xFF_task_user", null, [
      [ "Task_User", "classLab__0xFF__task__user_1_1Task__User.html", "classLab__0xFF__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0xFF_touch_panel", null, [
      [ "Touch_Panel", "classLab__0xFF__touch__panel_1_1Touch__Panel.html", "classLab__0xFF__touch__panel_1_1Touch__Panel" ]
    ] ]
];