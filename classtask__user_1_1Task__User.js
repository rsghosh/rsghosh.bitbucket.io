var classtask__user_1_1Task__User =
[
    [ "__init__", "classtask__user_1_1Task__User.html#a27487ead010434dad207dcf65fecfc78", null ],
    [ "print_readings", "classtask__user_1_1Task__User.html#a58df8308df755a85d8296f603b3b1998", null ],
    [ "reset_state", "classtask__user_1_1Task__User.html#a7b89e62213a351f40aafc7cca5770946", null ],
    [ "run", "classtask__user_1_1Task__User.html#a14dd7eb87f5946fdc577e6c8e84e9c9e", null ],
    [ "update_next_time", "classtask__user_1_1Task__User.html#aee93cde3d8f870f0cb207fc9a422838e", null ],
    [ "period", "classtask__user_1_1Task__User.html#a41e50f68adb46543e40b049bd7b3fcbb", null ],
    [ "pos_lst", "classtask__user_1_1Task__User.html#a50e99fe8ad3162dc6c0abcc94c1dbf8a", null ],
    [ "ser_port", "classtask__user_1_1Task__User.html#a3c12e4462b8f494a608e0e3458ecf3a0", null ],
    [ "shared_vars", "classtask__user_1_1Task__User.html#a5fbd1f02b1718f5c90bcecacb0e41957", null ],
    [ "started_collecting", "classtask__user_1_1Task__User.html#a87e82ed7b2cb144df22ad01c9aedac93", null ],
    [ "state", "classtask__user_1_1Task__User.html#afefb79be360ac39f0ed9920de91f953e", null ],
    [ "t_increment", "classtask__user_1_1Task__User.html#aae8238f6ee8f7b2f6d7bf78493cb169d", null ],
    [ "time_lst", "classtask__user_1_1Task__User.html#ae10af47d79f55ee640f1ac2c15855b8c", null ]
];