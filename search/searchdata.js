var indexSectionsWithContent =
{
  0: "_abcdefghiklmnoprstuwxyz",
  1: "cdeimt",
  2: "l",
  3: "hlm",
  4: "_cdefgmoprsuw",
  5: "abcdefiklmnprstuxyz",
  6: "ghl"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Pages"
};

