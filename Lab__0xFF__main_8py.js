var Lab__0xFF__main_8py =
[
    [ "imu", "Lab__0xFF__main_8py.html#a7ae63e0bc62c6d88f117c4ff93168f2c", null ],
    [ "in1", "Lab__0xFF__main_8py.html#ac1996981a0b0603579aa77abcd0f1fd8", null ],
    [ "in2", "Lab__0xFF__main_8py.html#a82f9cdcccda3447e6dd460b4b58da156", null ],
    [ "in3", "Lab__0xFF__main_8py.html#a8ef0a7c65eff17f60c9a8a909a2ff358", null ],
    [ "in4", "Lab__0xFF__main_8py.html#add8644e8434fb6c0770f8055017c000d", null ],
    [ "K1", "Lab__0xFF__main_8py.html#a2314b9e5a5a3953eba5dfb64df174fc5", null ],
    [ "K2", "Lab__0xFF__main_8py.html#a4095797cdbcc57f7a4883a4fbea0398c", null ],
    [ "motor_1", "Lab__0xFF__main_8py.html#a92debfc9d4a763f4b86b87fe9b144343", null ],
    [ "motor_2", "Lab__0xFF__main_8py.html#ab116af079299fe989352507d0b367bfc", null ],
    [ "motor_drv", "Lab__0xFF__main_8py.html#a4638bd579fd569e6f901f57576d26131", null ],
    [ "nFault", "Lab__0xFF__main_8py.html#a0ee1e8e77981c3a695770bc38d1df91e", null ],
    [ "nSleep", "Lab__0xFF__main_8py.html#a3f664ebc8a872831aeec02203465ce46", null ],
    [ "shared_vars", "Lab__0xFF__main_8py.html#a56f73bd071f43f8cf21c815d743838ba", null ],
    [ "t3ch1", "Lab__0xFF__main_8py.html#ab9d42eed89d975f53e01d10cb147dc01", null ],
    [ "t3ch2", "Lab__0xFF__main_8py.html#a96e9252a655a74990d2bc7d84793fa88", null ],
    [ "t3ch3", "Lab__0xFF__main_8py.html#a89e262c91879b1af81973e56fb83c477", null ],
    [ "t3ch4", "Lab__0xFF__main_8py.html#a76f35313d31878d18ab1b4e56a2db623", null ],
    [ "t_collection", "Lab__0xFF__main_8py.html#aa491d14b46f39e93bf099b3e0e1a3012", null ],
    [ "t_imu", "Lab__0xFF__main_8py.html#acfbea04c57006950c1f4db48b7ca0197", null ],
    [ "t_motor", "Lab__0xFF__main_8py.html#a7b766c03d0b2209545fb07ca0da39e90", null ],
    [ "t_touch_panel", "Lab__0xFF__main_8py.html#a5848619e8f529973e6ed7649cb6186b5", null ],
    [ "t_user", "Lab__0xFF__main_8py.html#a54dfefa52f342fe11c00909fc39fbe4d", null ],
    [ "tim3", "Lab__0xFF__main_8py.html#a4cf9a696745c54213cf0b058043c4533", null ],
    [ "tp", "Lab__0xFF__main_8py.html#a2ded5995585a9b7819576f82cf487da6", null ],
    [ "xm", "Lab__0xFF__main_8py.html#a029735be80bb3ee4d9c1ee40c4c943aa", null ],
    [ "xp", "Lab__0xFF__main_8py.html#a079c9f7e1aad72dda876b39489db54de", null ],
    [ "ym", "Lab__0xFF__main_8py.html#a2b69ba2fac8ebed592e09476547211aa", null ],
    [ "yp", "Lab__0xFF__main_8py.html#afdbbc8e32d9100f679a646b1933a1de3", null ]
];