var Lab__0xFF__drv8847_8py =
[
    [ "Lab_0xFF_drv8847.DRV8847", "classLab__0xFF__drv8847_1_1DRV8847.html", "classLab__0xFF__drv8847_1_1DRV8847" ],
    [ "Lab_0xFF_drv8847.Motor", "classLab__0xFF__drv8847_1_1Motor.html", "classLab__0xFF__drv8847_1_1Motor" ],
    [ "in1", "Lab__0xFF__drv8847_8py.html#a1a42f91acd32f9c457d87dd8940d65d7", null ],
    [ "in2", "Lab__0xFF__drv8847_8py.html#a7867b730fd655e87c8a20cc19171b14a", null ],
    [ "in3", "Lab__0xFF__drv8847_8py.html#a04c549790cb86141ff2ca96ded4090df", null ],
    [ "in4", "Lab__0xFF__drv8847_8py.html#a6525721fa4a414186b9b3cb1fe0bd0bb", null ],
    [ "motor_1", "Lab__0xFF__drv8847_8py.html#addebb8bb6dfb94ed95b10b10ad3084f1", null ],
    [ "motor_2", "Lab__0xFF__drv8847_8py.html#a0ed2819e5541c1f4fcf957fa5605902f", null ],
    [ "motor_drv", "Lab__0xFF__drv8847_8py.html#a7f5efe4b6fbc8da82c9cd5f7a79fa99f", null ],
    [ "nFault", "Lab__0xFF__drv8847_8py.html#ab09d3840a09515f3dcac184a67ad9000", null ],
    [ "nSleep", "Lab__0xFF__drv8847_8py.html#a47dc7688713d981807596c141480ca4b", null ],
    [ "t3ch1", "Lab__0xFF__drv8847_8py.html#ac3edf93255b72c8a2cb8bc5d52ca26a6", null ],
    [ "t3ch2", "Lab__0xFF__drv8847_8py.html#a45285c83dcd0116682f83bf47a12fba6", null ],
    [ "t3ch3", "Lab__0xFF__drv8847_8py.html#a3b3ab0e444252ba53535a895564f947f", null ],
    [ "t3ch4", "Lab__0xFF__drv8847_8py.html#a301d6875ac2cb132b30176dc834d4fa5", null ],
    [ "tim3", "Lab__0xFF__drv8847_8py.html#aeaed8235dd2f128aa90405ac8c582d22", null ]
];