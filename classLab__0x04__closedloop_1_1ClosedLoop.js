var classLab__0x04__closedloop_1_1ClosedLoop =
[
    [ "__init__", "classLab__0x04__closedloop_1_1ClosedLoop.html#a538a4b6bacf43595a85ded5d96db6e10", null ],
    [ "get_Kp", "classLab__0x04__closedloop_1_1ClosedLoop.html#a8b4322994434f1180f03e915c2b07bc4", null ],
    [ "set_Kp", "classLab__0x04__closedloop_1_1ClosedLoop.html#a8fe0fb175f5851bca6fb13dba1c93e67", null ],
    [ "update", "classLab__0x04__closedloop_1_1ClosedLoop.html#a0a1bb4e392aefde5f883292d347e0075", null ],
    [ "Kp", "classLab__0x04__closedloop_1_1ClosedLoop.html#aab2846dbfd6efb9022db675055849d72", null ],
    [ "max_pwm", "classLab__0x04__closedloop_1_1ClosedLoop.html#a201e3642de83c15455ef4c3203e4258f", null ]
];