var annotated_dup =
[
    [ "Lab_0x02_encoder", null, [
      [ "Encoder", "classLab__0x02__encoder_1_1Encoder.html", "classLab__0x02__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x02_task_encoder", null, [
      [ "Task_Encoder", "classLab__0x02__task__encoder_1_1Task__Encoder.html", "classLab__0x02__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab_0x02_task_user", null, [
      [ "Task_User", "classLab__0x02__task__user_1_1Task__User.html", "classLab__0x02__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0x03_drv8847", null, [
      [ "DRV8847", "classLab__0x03__drv8847_1_1DRV8847.html", "classLab__0x03__drv8847_1_1DRV8847" ],
      [ "Motor", "classLab__0x03__drv8847_1_1Motor.html", "classLab__0x03__drv8847_1_1Motor" ]
    ] ],
    [ "Lab_0x03_encoder", null, [
      [ "Encoder", "classLab__0x03__encoder_1_1Encoder.html", "classLab__0x03__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x03_task_encoder", null, [
      [ "Task_Encoder", "classLab__0x03__task__encoder_1_1Task__Encoder.html", "classLab__0x03__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab_0x03_task_motor", null, [
      [ "Task_Motor", "classLab__0x03__task__motor_1_1Task__Motor.html", "classLab__0x03__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab_0x03_task_user", null, [
      [ "Task_User", "classLab__0x03__task__user_1_1Task__User.html", "classLab__0x03__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0x04_closedloop", null, [
      [ "ClosedLoop", "classLab__0x04__closedloop_1_1ClosedLoop.html", "classLab__0x04__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab_0x04_drv8847", null, [
      [ "DRV8847", "classLab__0x04__drv8847_1_1DRV8847.html", "classLab__0x04__drv8847_1_1DRV8847" ],
      [ "Motor", "classLab__0x04__drv8847_1_1Motor.html", "classLab__0x04__drv8847_1_1Motor" ]
    ] ],
    [ "Lab_0x04_encoder", null, [
      [ "Encoder", "classLab__0x04__encoder_1_1Encoder.html", "classLab__0x04__encoder_1_1Encoder" ]
    ] ],
    [ "Lab_0x04_task_encoder", null, [
      [ "Task_Encoder", "classLab__0x04__task__encoder_1_1Task__Encoder.html", "classLab__0x04__task__encoder_1_1Task__Encoder" ]
    ] ],
    [ "Lab_0x04_task_motor", null, [
      [ "Task_Motor", "classLab__0x04__task__motor_1_1Task__Motor.html", "classLab__0x04__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab_0x04_task_user", null, [
      [ "Task_User", "classLab__0x04__task__user_1_1Task__User.html", "classLab__0x04__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0x05", "namespaceLab__0x05.html", [
      [ "IMU_Driver", "classLab__0x05_1_1IMU__Driver.html", "classLab__0x05_1_1IMU__Driver" ]
    ] ],
    [ "Lab_0xFF_closedloop", null, [
      [ "ClosedLoop", "classLab__0xFF__closedloop_1_1ClosedLoop.html", "classLab__0xFF__closedloop_1_1ClosedLoop" ]
    ] ],
    [ "Lab_0xFF_drv8847", null, [
      [ "DRV8847", "classLab__0xFF__drv8847_1_1DRV8847.html", "classLab__0xFF__drv8847_1_1DRV8847" ],
      [ "Motor", "classLab__0xFF__drv8847_1_1Motor.html", "classLab__0xFF__drv8847_1_1Motor" ]
    ] ],
    [ "Lab_0xFF_imu_driver", null, [
      [ "IMU_Driver", "classLab__0xFF__imu__driver_1_1IMU__Driver.html", "classLab__0xFF__imu__driver_1_1IMU__Driver" ]
    ] ],
    [ "Lab_0xFF_task_collection", null, [
      [ "Task_Collection", "classLab__0xFF__task__collection_1_1Task__Collection.html", "classLab__0xFF__task__collection_1_1Task__Collection" ]
    ] ],
    [ "Lab_0xFF_task_imu", null, [
      [ "Task_IMU", "classLab__0xFF__task__imu_1_1Task__IMU.html", "classLab__0xFF__task__imu_1_1Task__IMU" ]
    ] ],
    [ "Lab_0xFF_task_motor", null, [
      [ "Task_Motor", "classLab__0xFF__task__motor_1_1Task__Motor.html", "classLab__0xFF__task__motor_1_1Task__Motor" ]
    ] ],
    [ "Lab_0xFF_task_touch_panel", null, [
      [ "Task_Touch_Panel", "classLab__0xFF__task__touch__panel_1_1Task__Touch__Panel.html", "classLab__0xFF__task__touch__panel_1_1Task__Touch__Panel" ]
    ] ],
    [ "Lab_0xFF_task_user", null, [
      [ "Task_User", "classLab__0xFF__task__user_1_1Task__User.html", "classLab__0xFF__task__user_1_1Task__User" ]
    ] ],
    [ "Lab_0xFF_touch_panel", null, [
      [ "Touch_Panel", "classLab__0xFF__touch__panel_1_1Touch__Panel.html", "classLab__0xFF__touch__panel_1_1Touch__Panel" ]
    ] ]
];