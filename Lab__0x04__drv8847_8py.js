var Lab__0x04__drv8847_8py =
[
    [ "Lab_0x04_drv8847.DRV8847", "classLab__0x04__drv8847_1_1DRV8847.html", "classLab__0x04__drv8847_1_1DRV8847" ],
    [ "Lab_0x04_drv8847.Motor", "classLab__0x04__drv8847_1_1Motor.html", "classLab__0x04__drv8847_1_1Motor" ],
    [ "in1", "Lab__0x04__drv8847_8py.html#acbeec04696c297e305c3179952c2848a", null ],
    [ "in2", "Lab__0x04__drv8847_8py.html#ae1172dcc2b509267aebde33e1d80b818", null ],
    [ "in3", "Lab__0x04__drv8847_8py.html#a7473cd75f901f19b5f38e240da5055fd", null ],
    [ "in4", "Lab__0x04__drv8847_8py.html#a188bb4f6c1893777c317d20bfccc7dc3", null ],
    [ "motor_1", "Lab__0x04__drv8847_8py.html#ab650d0258a54ef590c8beaeb65f772ab", null ],
    [ "motor_2", "Lab__0x04__drv8847_8py.html#a1601d21feba546ba835e3c95c5a349ca", null ],
    [ "motor_drv", "Lab__0x04__drv8847_8py.html#ab0e3564816bb7d50da2dc9c70e238795", null ],
    [ "nFault", "Lab__0x04__drv8847_8py.html#a73f085c1d1c914a0acc105531b6332a2", null ],
    [ "nSleep", "Lab__0x04__drv8847_8py.html#a18f7d54743f9916893e5fa33b8eb3eef", null ],
    [ "t3ch1", "Lab__0x04__drv8847_8py.html#a8d5390af465e10e5f5ae0cdff80c4a80", null ],
    [ "t3ch2", "Lab__0x04__drv8847_8py.html#ae00b8243f7a4298adbffed5c6cf0d1a3", null ],
    [ "t3ch3", "Lab__0x04__drv8847_8py.html#a4d501548b883818cc480a623e8e9036d", null ],
    [ "t3ch4", "Lab__0x04__drv8847_8py.html#a3e8dddcd377623388da546d8e156cb3d", null ],
    [ "tim3", "Lab__0x04__drv8847_8py.html#a54051e5864825727991b66f6e0d223c9", null ]
];